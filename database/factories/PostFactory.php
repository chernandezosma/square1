<?php

namespace Database\Factories;

use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        User::factory()->create();
        $user = User::firstOrFail();

        return [
            'author_id' => $user->id,
            'title' => $this->faker->text(200),
            'description' => $this->faker->text(),
            'active' => rand(0,1)
        ];
    }
}
