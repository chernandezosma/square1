<?php

namespace Database\Seeders;

use App\Models\Post;
use App\Models\User;
use DateTime;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Seed the admin user
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@square1.io',
            'email_verified_at' => new Datetime(),
            'password' => Hash::make('password'),
            'remember_token' => Str::random(10),
            'created_at' => new Datetime(),
            'updated_at' => new Datetime()
        ]);

        // Other seeds needed to testing.
        Post::factory()->count(50)->create();
    }
}
