<?php

use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [PostController::class, 'home'])->name('home');
Route::get('/all-post', [PostController::class, 'showAllPosts'])->name('show-all');
Route::get('/login', [UserController::class, 'login'])->name('login');
Route::post('/signin', [UserController::class, 'signin'])->name('signin');
Route::get('/register', [UserController::class, 'register'])->name('register');
Route::post('/signup', [UserController::class, 'signup'])->name('signup');
Route::get('post/{slug}', [PostController::class, 'show'])->where('slug', '[A-Za-z0-9-_]+')->name('view-post');
//Route::get('consume', [PostController::class, 'getResultFromAPI'])->name('consume-api');

Route::middleware(['auth'])->group(function () {
    Route::get('/logout', [UserController::class, 'logout'])->name('logout');
    Route::get('new-post', [PostController::class, 'newPost'])->name('new-post');
    Route::post('persist-post', [PostController::class, 'persistPost'])->name('save-post');
    Route::get('user/{id}', [UserController::class, 'profile'])->where('id', '[0-9]+')->name('user-profile');
    Route::get('posts/user/{id}', [PostController::class, 'myPosts'])->where('id', '[0-9]+')->name('my-posts');
});