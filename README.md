# Square1 - Web Developer.

This document contains the information about the test and how to play it.

## Technology
To write the exercise, I use the following tools: 
- I use Laravel 8, even when I know that it is not an LTS version
- MySQL 5.7 
- Docker to join all servers

The project has the following folder structure: 

```
Square1
    |-- app
    |-- bootstrap
    |-- config
    |-- database
    |-- docker  
        |-- etc  
            |-- db  
                |-- mysql
                    |-- conf  
                    |-- data  
                    |-- init  
                    |-- logs  
            |-- php  
            |-- web  
        |-- docker-compose.yml  
        |-- .env
    |-- public  
    |-- resources
    |-- routes
    |-- storage
    |-- tests
    |-- vendor  
```

All docker's infrastructure configuration is contained into docker folder, and the particular data is defined at _.env_ file but due to restrictions on docker deploy files, is not possible to replicate all information on some files like containers' DockerFiles, so should beware if you change anything on _.env_ file because maybe you must propagate manually the changes on those files.

In that folder structure, I have separate the servers into several folders in order to keep the data isolated for each container, and depends on the container may require more or less folders.

Regarding to the Laravel's folders, I have followed the Laravel's way and I have kept the original structure.

To startup the project you need to follow the steps at [How to play](#How-to-play), with project chapter.

## How to play

Before start the project, please read carefully the [Considerations](#Considerations) section.
 
These are the steps to run the project into your system.

- Clone the repository: `git clone https://gitlab.com/elestadoweb.com/square1.git`
- Go to square1/docker and run `docker up --build`
- Set the appropriate permissions to _bootstrap_ and _storage_ folders running the following commands inside de container
    - `chgrp -R www-data storage bootstrap/cache`
    - `chmod -R ug+rwx storage bootstrap/cache`
- Go to docker folder and run `docker-compose up`
- You must run the following artisan commands, from the project root folder, in order to setup the database
    - `php artisan migrate:install --env=local`
    - `php artisan migrate:fresh --env=local --seed`
- Open your favorite browser and go to `http://localhost:8080/`

## Considerations

### What you need to run the project:

in order to run the project you need to have installed the following software:   

- [Docker Desktop](https://www.docker.com/products/docker-desktop)

All project run into Docker containers, so all _artisan_ commands must be run inside the PHP container, that is `square1_php`.

To get into that conatiner, you , so to get in on it, you must run the following command:

    `docker exec -it square1_php bash`

you can run directly inside the container with the following way

    `docker exec -it square1_php php artisan migrate:install --env=local`

Could you need to appy a `chmod -R 775 to docker/etc/db/mysql` in order to change the permissions to the files, because sometimes they are created under an unknown userid and it crashes the docker container build process.

## Speed up the project

As the test require the server should be able to support thousand of maybe millions request per second, and after study the possible solutions I reach these conclusions. 

At code level, we can cache config and routes loading and use, but is not recommended cache the query results itself because there is no unique cache-key to determine when the cache content should be updated.

If we use cache at project level, we get a big delay or even the results don't be refreshed when it will be added or refreshed at front-end.

If we want to speed up the project the most used solution is to use an cluster of web servers, by instance, Apache, and a Nginx in back processing the request from the cluster.

Above this solution, we can implement a combination of Redis and Varnish to cache the results. Even we can store the post it selves in a no-sql DB which speed up the searches and retrieves a lot.  

And as top level solution all above can be implemented in a Kubernetes in order to deploy instances as needed bassed on the traffic that the side is reciving, but this solution in cases of a huge traffic could be so expensive, so we should need to study the metrics in order to determine which is the best implementation.

