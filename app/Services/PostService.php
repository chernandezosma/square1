<?php

namespace App\Services;

use App\Http\Requests\PostFormRequest;
use App\Models\Post;
use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Throwable;

/**
 * Service for method Post related
 *
 * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
 * @version Nov 2020
 *
 */
class PostService
{
    const URL = 'https://sq1-api-test.herokuapp.com/posts';

    /**
     * @var string
     */
    private $url;

    /**
     * @var Guzzle Client
     */
    private $client;

    /**
     * PostService constructor.
     *
     * @param Client|null $client
     * @param string|null $url
     */
    public function __construct(Client $client = null, string $url = null)
    {
        $this->url = $url ?? PostService::URL;
        $this->client = $client ?? new Client();
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return Guzzle
     */
    public function getClient(): Guzzle
    {
        return $this->client;
    }

    /**
     * @param Guzzle $client
     */
    public function setClient(Guzzle $client): void
    {
        $this->client = $client;
    }

    private function getAdminUser()
    {
        return User::where('name', 'admin')->first();
    }

    /**
     * Method which call the endpoint to get the new messages.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2020
     *
     */
    public function getResultFromAPI()
    {
        $adminUser = $this->getAdminUser();
        $postRules = (new PostFormRequest())->rules();
        try {
            $response = $this->client->request('GET', $this->url, [
                'verify'  => false,
            ]);
            if (!$response->getStatusCode() !== 200) {
                $responseBody = json_decode($response->getBody());
                foreach ($responseBody->data as $record) {
                    $validator = Validator::make(json_decode(json_encode($record), true), $postRules);
                    if ($validator->passes()) {
                        $post = new Post();
                        $post->author_id = $adminUser->id;
                        $post->title = $record->title;
                        $post->description = $record->description;
                        $post->publish_date = $record->publication_date;
                        $post->active = true;
                        $post->saveOrFail();
                    } else {
                        $recordForLog = explode('|', $record);
                        Log::error(sprintf ('The post [%s] has not pass the validation rules.', $recordForLog));
                    }
                }
            }
        } catch (Throwable $e) {
            report($e);
        }
    }

}