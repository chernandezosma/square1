<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\PostService;

class ImportFromApi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'post:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import posts from API endpoint';

    /**
     *
     * @var PostService
     */
    protected $postService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Postservice $postService)
    {
        parent::__construct();
        $this->postService = $postService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->postService->getResultFromAPI();

        return 0;
    }
}
