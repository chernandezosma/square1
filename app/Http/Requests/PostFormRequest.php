<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class PostFormRequest extends FormRequest
{
    /**
     * Return true if it met all authorization rules.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2020
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * Rules to validate the post. We add a validation to the description length
     * because the post could be huge and make an error during insert.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2020
     *
     * @return string[]
     */
    public function rules()
    {
        return [
            'title' => 'required|unique:posts|max:255',
            'description' => 'required|max:65535',
        ];
    }
}