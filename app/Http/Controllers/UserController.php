<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginFormRequest;
use App\Http\Requests\RegisterFormRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Throwable;

class UserController extends Controller
{
    /**
     * Show the login page
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2020
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function login(Request $request)
    {
        return view('sections.login', ['title' => 'Login']);
    }

    /**
     * Do the login process and show erroes if they are present.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2020
     *
     * @param LoginFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function signin(LoginFormRequest $request)
    {
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            return redirect()->route('home');
        }

        // if unsuccessful -> redirect back
        return redirect()
            ->back()
            ->withInput($request->only('email'))
            ->withErrors([
                'errors' => 'You are not using a valid credentials.'
            ]);
    }

    /**
     * Show a register page.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2020
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function register(Request $request)
    {
        return view('sections.register', ['title' => 'Register']);
    }

    /**
     * Persist the user given in a register page. if during the process
     * any error happend, it will be shown.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2020
     *
     * @param RegisterFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function signup(RegisterFormRequest $request)
    {
        $user = new User($request->only('name', 'email', 'password'));

        try {
            $user->saveOrFail();
            Auth::login($user);

            return redirect()->route('home');
        } catch (Throwable $e) {
            return redirect()
                ->back()
                ->withInput(['name', 'email'])
                ->withErrors([
                    'error' => $e->getMessage()
                ]);
        }
    }

    /**
     * Do the logout process.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2020
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout(Request $request)
    {
        Auth::logout();

        return redirect()->route('home');
    }
}
