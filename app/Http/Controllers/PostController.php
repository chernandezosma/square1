<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostFormRequest;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Throwable;

class PostController extends Controller
{
    /**
     * Home page where the all post are listed, if there is no user logged.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2020
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function home()
    {
        if (Auth::check()) {
            $userId = Auth::User()->id;
            $posts = $this->getMyOwnPosts($userId);
        } else {
            $posts = $this->getAllPosts();
        }

        return view('sections.home', [
            'posts' => $posts,
            'title' => 'Latest Posts',
        ]);
    }

    /**
     * Show all posts even when there are a user logged.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2020
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function showAllPosts()
    {
        return view('sections.home', [
            'posts' => $this->getAllPosts(),
            'title' => 'Latest Posts',
        ]);
    }

    /**
     * Show the form to add new post.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2020
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function newPost()
    {
        return view('sections.post', [
            'title' => 'Add new post',
        ]);
    }

    /**
     * Persist the post into database, if all goes well return to home,
     * otherwise throw an error and show it in the new-post page.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2020
     *
     * @param PostFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function persistPost(PostFormRequest $request)
    {
        $post = new Post($request->only('title', 'description'));
        $post->active = true;
        $post->author_id = Auth::user()->id;
        try {
            $post->saveOrFail();

            return redirect()->route('home');
        } catch (Throwable $e) {
            return redirect()
                ->back()
                ->withInput(['title', 'description'])
                ->withErrors([
                    'error' => $e->getMessage(),
                ]);
        }
    }

    /**
     * Show the selected given post.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2020
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function show(Request $request)
    {
        $post = Post::where('slug', $request->slug)->first();

        if (!$post) {
            return redirect()
                ->route('home')
                ->with('error', 'The post could not be found');
        }

        return view('sections.showPost', [
            'post'  => $post,
            'title' => $post->title,
        ]);
    }

    /**
     * Get all user own posts. Refactored to be cached.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2020
     *
     * @param int $userId
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    private function getMyOwnPosts(int $userId)
    {
        return Post::where('active', 1)
                   ->where('author_id', $userId)
                   ->with('author')
                   ->orderBy('publish_date', 'desc')
                   ->paginate(5);
    }

    /**
     * Extract the query to a primitive function.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2020
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    private function getAllPosts()
    {
        return Post::where('active', 1)
                   ->with('author')
                   ->orderBy('publish_date', 'desc')
                   ->paginate(5);
    }

}
