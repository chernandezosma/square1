<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta author="Cayetano H. Osma">
    <title>@yield('title')</title>
    <link href={{ asset('assets/css/bootstrap.min.css') }} rel="stylesheet">
</head>
<body class="antialiased">
    <div class="container-fluid">
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h1 class="display-4">@yield('title')</h1>
                <p class="lead">Blog Platform.</p>
            </div>
        </div>
        <div class="container-fluid">
            @include('includes.menu')
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-10">@yield('content')</div>
                <div class="col-2">
                    @include ('includes.statistics')
                </div>
            </div>
        </div>
    </div>

    <script
            src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
            crossorigin="anonymous"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
</body>
</html>