<hr />
<div class="row">
    <ul class="nav mr-auto">
        <li class="nav-item">
            <a class="nav-link" href="{{ route('home') }}">Home</a>
        </li>
    </ul>
    <ul class="nav ml-auto">
        @if (!Auth::guest())
            <li class="nav-item">
                <a class="nav-link active" href="{{ route('new-post') }}">New post</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" href="{{ route('show-all') }}" alt="Show all posts">Show all posts</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" href="{{ route('home') }}" alt="Show all posts">Show my posts</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" href="{{ route('logout') }}#">Logout</a>
            </li>
        @else
            <li class="nav-item">
            <a class="nav-link" href="{{ route('register') }}">Register</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">Login</a>
            </li>
        @endif
    </ul>
</div>
<hr />


