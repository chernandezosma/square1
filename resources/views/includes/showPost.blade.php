<div class="col-12">
    <div class="card mt-2 mx-auto">
        <div class="card-body">
            <h5 class="card-title">
                <a class="h2" href="{{ route('view-post', $post->slug) }}" alt="view-post">{{ $post->title }}</a>
            </h5>
            <p class="card-text">{{ mb_strimwidth($post->description, 0, 250, "...") }}</p>
            <div class="float-right">
                <h6 class="card-subtitle text-muted">
                    {{ $post->author->name }}, {{ $post->publish_date }}
                </h6>
            </div>
        </div>
    </div>
</div>