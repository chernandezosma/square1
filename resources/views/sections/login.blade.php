@extends('layouts.master')

@section('title', $title)

@section('content')
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            @if($errors->any())
                <div class="alert alert-light">
                    <ul class="list-group">
                        @foreach($errors->all() as $error)
                            <li class="list-group-item list-group-item-danger">{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            {{ Form::open(array('route' => 'signin')) }}
                <div class="form-group">
                    <div class="form-group">
                        {{ Form::email('email', null, [
                            'placeholder' => 'Enter your user email',
                            'class' => 'form-control',
                        ]) }}
                    </div>
                    <div class="form-group">
                        {{ Form::password('password', [
                            'placeholder' => 'Type your password',
                            'class' => 'form-control'
                        ]) }}
                    </div>
                    {{ Form::submit('Login', ['class' => 'btn btn-primary float-right']) }}
                </div>
            {{ Form::close() }}
        </div>
        <div class="col-md-3"></div>
    </div>
@endsection