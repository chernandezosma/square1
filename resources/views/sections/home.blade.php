@extends('layouts.master')
@section('title', $title)
@section('content')

    <div class="row">
        <div class="col-12">
            @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ $message }}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick="$().alert('close')">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
        </div>
    </div>

    @if (!is_null($posts))
        @forelse ($posts as $post)
            <div class="row">
                @include('includes.showPost', ['posts' => $post])
            </div>
        @empty
            <div class="alert alert-dark" role="alert">
                There is no post to show.
            </div>
        @endforelse
        <div class="mt-2 d-flex flex-row-reverse">
            {{ $posts->links() }}
        </div>
    @else
        <div class="alert alert-dark" role="alert">
            There is no post to show.
        </div>
    @endif
@endsection