@extends('layouts.master')

@section('title', $title)

@section('content')
    <div class="row">
        @include('includes.showPost', ['posts' => $post])
    </div>
@endsection