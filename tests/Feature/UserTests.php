<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class UserTests extends TestCase
{
    const EMAIL = 'admin@square1.io';
    const PASSWORD = 'A123456789BZX';

    use RefreshDatabase;

    /**
     * @var Post[]|\Illuminate\Database\Eloquent\Collection
     */
    protected $posts;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * Check if is possible to store and to recover a User from database.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2020
     *
     */
    public function testCreateUser()
    {
        User::factory()->create();

        $user = User::all();
        $this->assertNotEmpty($user);
    }

    /**
     * Check if is possible to store and to recover a specific User from database.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2020
     *
     */
    public function testCreateSpecificUser()
    {
        User::factory()->create(['email' => UserTests::EMAIL]);

        $user = User::where('email', UserTests::EMAIL)->first();

        $this->assertNotEmpty($user);
    }

    /**
     * Check user Login
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2020
     *
     */
    public function testLogin()
    {
        User::factory()->create(['email' => UserTests::EMAIL, 'password' => UserTests::PASSWORD]);

        $result = Auth::attempt(['email' => UserTests::EMAIL, 'password' => UserTests::PASSWORD]);

        $this->assertTrue($result);
    }

    /**
     * Check user Login failed
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2020
     *
     */
    public function testFailedLogin()
    {
        User::factory()->create(['email' => UserTests::EMAIL, 'password' => UserTests::PASSWORD]);

        $result = Auth::attempt(['email' => UserTests::EMAIL, 'password' => 'ZXS123456789ASD']);

        $this->assertFalse($result);
    }

    /**
     * Check logout
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2020
     *
     */
    public function testLogout()
    {
        User::factory()->create(['email' => UserTests::EMAIL, 'password' => UserTests::PASSWORD]);
        $result = Auth::attempt(['email' => UserTests::EMAIL, 'password' => UserTests::PASSWORD]);

        Auth::logout();

        $result = Auth::check();

        $this->assertFalse($result);
    }
}
