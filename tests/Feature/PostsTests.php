<?php

namespace Tests\Feature;

use App\Models\Post;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PostsTests extends TestCase
{
    use RefreshDatabase;

    /**
     * @var Post[]|\Illuminate\Database\Eloquent\Collection
     */
    protected $posts;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * Check if is possible to recover the Posts in database.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2020
     *
     */
    public function testGetAllPosts()
    {
        Post::factory()->count(10)->create();

        $posts = Post::all();
        $this->assertNotEmpty($posts);
    }

    /**
     * Check if it recover 10 posts.
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2020
     *
     */
    public function testGetTenPosts()
    {
        Post::factory()->count(10)->create();

        $posts = Post::all();
        $this->assertCount(10, $posts);
    }

    /**
     * Capture the exception when title is empty
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2020
     */
    public function testTitleEmptyPosts()
    {
        $this->expectException(QueryException::class);
        $this->expectExceptionMessageMatches('/.*Integrity constraint violation: 1048 Column .* cannot be null.*/');
        $post = Post::factory()->create(['title' => NULL]);
    }

    /**
     * Capture the exception when title is empty
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2020
     */
    public function testAuthorEmptyPosts()
    {
        $this->expectException(QueryException::class);
        $this->expectExceptionMessageMatches('/.*Integrity constraint violation: 1048 Column .* cannot be null.*/');
        $post = Post::factory()->create(['author_id' => NULL]);
    }

    /**
     * Capture the exception when title is empty
     *
     * @author  Cayetano H. Osma <chernandez@elestadoweb.com>
     * @version Nov 2020
     */
    public function testTitleNonUnique()
    {
        $this->expectException(QueryException::class);
        $this->expectExceptionMessageMatches('/.*Integrity constraint violation\: 1062 Duplicate entry.*/');
        $posts = Post::factory()->count(2)->create(['title' => 'I killed her for a yogurt']);
    }
}
